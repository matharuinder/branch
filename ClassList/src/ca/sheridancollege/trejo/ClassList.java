
package ca.sheridancollege.trejo;

import java.util.ArrayList;
import java.util.List;
/**
 * A class that is meant to be used for practicing the skills necessary to branch/merge and pull/push
 * to a Git repository.
 * For ICE 4, please add one class that is named after your Capstone Group
 * and add one instance of that class to my ArrayList. If you wish to override the 
 * toString method in your new class, you may.
 * @author Liz Dancy, 2017.
 * @author Ramses Trejo, 2020
 *
 */
public class ClassList {

	public static void main(String[] args) 
	{
		List<Person> classList = new ArrayList<Person>();



		classList.add(new Person("Instructor", "Liz Dancy"));
		classList.add(new Person("Instructor", "Ramses Trejo"));
		classList.add(new Eman("Student", "Eman Butt"));
		classList.add(new Dilraz("Student","Dilraz Singh Saini"));
		classList.add(new Person("Assistant","Kevin"));
		

		System.out.println(classList);


	}

}

